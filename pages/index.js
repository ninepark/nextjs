import styles from '../styles/Home.module.css'

export default function Home() {
  return (
    <div className={styles.container}>
      <main>
        <section className="container" id="demo-content">
          <h1 className="title">Scan 1D/2D Code from Video Camera</h1>

          <div>
            <a className="button" id="startButton" style={{marginRight: "10px"}}>Start</a>
            <a className="button" id="resetButton">Reset</a>
          </div>

          <div>
            <video id="video" width="300" height="200" style={{border: "1px solid gray"}}></video>
          </div>

          <div id="sourceSelectPanel" style={{display:"none"}}>
            <label htmlFor="sourceSelect">Change video source:</label>
            <select id="sourceSelect" style={{maxWidth:"400px"}}>
            </select>
          </div>

          <label>Result:</label>
          <pre><code id="result"></code></pre>

        </section>

        <script type="text/javascript" src="https://unpkg.com/@zxing/library@latest"></script>
        <script src="static/qrCodeScanner.js"></script>
      </main>
    </div>
  )
}
