import '../styles/globals.css'
import Head from "next/head";
import '../src/styles.css';

function MyApp({ Component, pageProps }) {
  return (
      <>
        <Head>
            <meta charSet="utf-8"/>
            <meta name="viewport" content="width=device-width, initial-scale=1"/>
            <meta name="author" content="ZXing for JS"/>
            <title>ZXing TypeScript | Decoding from camera stream</title>
            <link rel="stylesheet" rel="preload" as="style" onLoad="this.rel='stylesheet';this.onload=null" href="https://fonts.googleapis.com/css?family=Roboto:300,300italic,700,700italic"/>
            <link rel="stylesheet" rel="preload" as="style" onLoad="this.rel='stylesheet';this.onload=null" href="https://unpkg.com/normalize.css@8.0.0/normalize.css"/>
            <link rel="stylesheet" rel="preload" as="style" onLoad="this.rel='stylesheet';this.onload=null" href="https://unpkg.com/milligram@1.3.0/dist/milligram.min.css"/>
        </Head>
        <Component {...pageProps} />
      </>
  )
}

export default MyApp
